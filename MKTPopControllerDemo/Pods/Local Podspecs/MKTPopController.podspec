Pod::Spec.new do |s|
  s.name         = "MKTPopController"
  s.version      = "0.0.1"
  s.summary      = "MKTPopController is simple controller of popup view."
  s.homepage     = "https://bitbucket.org/ysknkd/"
  s.license      = 'MIT'
  s.author       = 'Yosuke Nakada'
  s.platform     = :ios, '7.0'
  s.source       = { :git => "https://bitbucket.org/ysknkd/mktpopcontroller.git", :tag => "0.0.1" }
  s.source_files  = 'MKTPopController', 'Classes/**/*.{h,m}'
  s.requires_arc = true
end
