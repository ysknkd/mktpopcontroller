//
//  UIView+Capture.h
//  Custom Pop Controller for iPhone
//
//  Created by Yosuke Nakada on 2/4/14.
//
//

#import <UIKit/UIKit.h>

@interface UIView (Capture)

- (void)captureViewWithCompletion:(void (^)(UIImage* capturedImage))completionHandler;
- (UIImage*)captureView;

@end
