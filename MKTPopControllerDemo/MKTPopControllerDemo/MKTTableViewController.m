//
//  MKTTableViewController.m
//  MKTPopControllerDemo
//
//  Created by Yosuke Nakada on 2/5/14.
//
//

#import "MKTTableViewController.h"
#import "MKTDatePickerViewController.h"

#import "MKTPopController.h"

static NSString* dateCellIdentifier = @"dateCell";

@interface MKTTableViewController ()

@property (nonatomic, weak) IBOutlet UITableViewCell* startDate;
@property (nonatomic, weak) IBOutlet UITableViewCell* endDate;
@property (nonatomic) NSDateFormatter* dateFormatter;

@property (nonatomic) NSMutableArray* settings;

@property (nonatomic) MKTPopController* popController;

@end

@implementation MKTTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSDate* date = [NSDate date];
    self.dateFormatter = [[NSDateFormatter alloc] init];
    [_dateFormatter setDateStyle:NSDateFormatterShortStyle];
    [_dateFormatter setTimeStyle:NSDateFormatterNoStyle];
    
    [_startDate.detailTextLabel setText:[_dateFormatter stringFromDate:date]];
    [_endDate.detailTextLabel setText:[_dateFormatter stringFromDate:date]];

    self.settings = [@[
                       [@{@"date": date} mutableCopy],
                       [@{@"date": date} mutableCopy]]
                     mutableCopy];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Utilities
- (NSMutableDictionary*)settingsWithIndexPath:(NSIndexPath*)indexPath
{
    return [_settings objectAtIndex:indexPath.row];
}

- (void)popDatePickerWithTableView:(UITableView*)tableView cell:(UITableViewCell*)cell indexPath:(NSIndexPath*)indexPath
{
    NSMutableDictionary* dict = [self settingsWithIndexPath:indexPath];
    MKTDatePickerViewController* datePickerController = [[MKTDatePickerViewController alloc] init];
    
    [datePickerController setDate:[dict objectForKey:@"date"]];
    
    self.popController = [[MKTPopController alloc] init];
//    [_popController setViewMargin:0.0f];
//    [_popController setViewCornerRadius:0.0f];
    [_popController setContentsViewController:datePickerController
                                     withSize:CGSizeMake(self.view.bounds.size.width, 180.0f)];
    
    __weak MKTTableViewController* selfRef = self;
    [_popController presentPopControllerWithCompletion:nil DismissHandler:^{
        NSString* dateString = [selfRef.dateFormatter stringFromDate:datePickerController.date];
        [cell.detailTextLabel setText:dateString];
        
        [dict setObject:datePickerController.date forKey:@"date"];
        selfRef.settings[indexPath.row] = dict;
        selfRef.popController = nil;
        
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }];
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
    if ([cell.reuseIdentifier isEqualToString:dateCellIdentifier]) {
        [self popDatePickerWithTableView:tableView cell:cell indexPath:indexPath];
    }
}

@end
