//
//  main.m
//  MKTPopControllerDemo
//
//  Created by Yosuke Nakada on 2/6/14.
//  Copyright (c) 2014 Yosuke Nakada. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MKTAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MKTAppDelegate class]));
    }
}
