//
//  MKTAppDelegate.h
//  MKTPopControllerDemo
//
//  Created by Yosuke Nakada on 2/6/14.
//  Copyright (c) 2014 Yosuke Nakada. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MKTAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
