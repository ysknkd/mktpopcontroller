//
//  MKTDatePickerViewController.h
//  MKTPopControllerDemo
//
//  Created by Yosuke Nakada on 2/4/14.
//
//

#import <UIKit/UIKit.h>

@interface MKTDatePickerViewController : UIViewController

@property (nonatomic) NSDate* date;

@end
