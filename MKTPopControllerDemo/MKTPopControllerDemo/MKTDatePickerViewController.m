//
//  MKTDatePickerViewController.m
//  MKTPopControllerDemo
//
//  Created by Yosuke Nakada on 2/4/14.
//
//

#import "MKTDatePickerViewController.h"

@interface MKTDatePickerViewController ()
@property (nonatomic) UIDatePicker* datePicker;
@end

@implementation MKTDatePickerViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    if (_date == nil) {
        self.date = [NSDate date];
    }
    self.datePicker = [[UIDatePicker alloc] initWithFrame:CGRectZero];
    [_datePicker setDate:_date];
    [_datePicker setDatePickerMode:UIDatePickerModeDate];
    [_datePicker setBackgroundColor:[UIColor whiteColor]];
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:_datePicker];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [_datePicker addTarget:self action:@selector(didChangeDatePicker:) forControlEvents:UIControlEventValueChanged];
    
    CGFloat width = self.view.bounds.size.width;
    CGFloat height = self.view.bounds.size.height;
    [_datePicker setFrame:CGRectMake(0, 0, width, height)];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [_datePicker removeTarget:self action:@selector(didChangeDatePicker:) forControlEvents:UIControlEventValueChanged];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];

}

#pragma -mark Events
- (void)didChangeDatePicker:(id)sender
{
    _date = _datePicker.date;
}

@end
