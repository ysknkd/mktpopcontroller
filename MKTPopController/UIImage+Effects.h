//
//  UIImage+Effects.h
//  Custom Pop Controller for iPhone
//
//  Created by Yosuke Nakada on 2/4/14.
//
//

#import <UIKit/UIKit.h>

@interface UIImage (Effects)

- (UIImage*)blurredImageWithRadius:(CGFloat)radius;

@end
