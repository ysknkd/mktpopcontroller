//
//  UIImage+Effects.m
//  Custom Pop Controller for iPhone
//
//  Created by Yosuke Nakada on 2/4/14.
//
//

#import "UIImage+Effects.h"

#ifdef USE_GPU_IMAGE
#import "GPUImage.h"
#endif

@implementation UIImage (Effects)


- (UIImage*)blurredImageWithRadius:(CGFloat)radius
{
#ifdef USE_GPU_IMAGE
    GPUImagePicture* srcImage = [[GPUImagePicture alloc] initWithImage:self smoothlyScaleOutput:YES];
    GPUImageBoxBlurFilter* filter = [[GPUImageBoxBlurFilter alloc] init];
    [filter setBlurRadiusInPixels:radius];
    
    [srcImage addTarget:filter];
    [srcImage processImage];
    
    [[UIDevice currentDevice] endGeneratingDeviceOrientationNotifications];
    UIImage* blurredImage = [filter imageFromCurrentlyProcessedOutput];
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    
    return blurredImage;
#else
    CIImage* inputImage = [[CIImage alloc] initWithImage:self];
    CIFilter* blurFilter = [CIFilter filterWithName:@"CIGaussianBlur"];
    [blurFilter setDefaults];
    [blurFilter setValue:inputImage forKey:@"inputImage"];
    [blurFilter setValue:[NSNumber numberWithFloat:radius] forKey:@"inputRadius"];
    
    // why will memory leak ... ?
    UIImage* blurredImage = [UIImage imageWithCIImage:blurFilter.outputImage];
    
    return blurredImage;
#endif
}

@end
