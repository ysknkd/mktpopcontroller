//
//  UIView+Capture.m
//  Custom Pop Controller for iPhone
//
//  Created by Yosuke Nakada on 2/4/14.
//
//

#import "UIView+Capture.h"

@implementation UIView (Capture)

- (void)captureViewWithCompletion:(void (^)(UIImage* capturedImage))completionHandler
{
    CGSize size = self.bounds.size;
    CALayer* __weak layer = self.layer;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        UIGraphicsBeginImageContext(size);
        CGContextRef context = UIGraphicsGetCurrentContext();
        [layer renderInContext:context];

        UIImage* image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        dispatch_async(dispatch_get_main_queue(), ^{
            completionHandler(image);
        });
    });
}

- (UIImage*)captureView
{
    UIGraphicsBeginImageContext(self.bounds.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [self.layer renderInContext:context];
    
    UIImage* image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

@end
