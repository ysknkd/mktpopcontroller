//
//  MKTPopController.h
//  Custom Pop Controller for iPhone
//
//  Created by Yosuke Nakada on 1/21/14.
//
//

#import <UIKit/UIKit.h>

typedef void (^MKTCompletionHandler)(void);
typedef void (^MKTDismissHandler)(void);

@interface MKTPopController : NSObject

@property (nonatomic) CGFloat viewMargin;
@property (nonatomic) CGFloat viewCornerRadius;
@property (nonatomic) CGFloat captureMargin;

- (void)setContentsViewController:(UIViewController*)viewController withSize:(CGSize)size;
- (void)presentPopControllerWithCompletion:(MKTCompletionHandler)completionHandler
                            DismissHandler:(MKTDismissHandler)dismissHandler;
- (void)dismissPopController;

@end
