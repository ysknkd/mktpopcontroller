//
//  MKTPopController.m
//  Custom Pop Controller for iPhone
//
//  Created by Yosuke Nakada on 1/21/14.
//
//

#import "MKTPopController.h"
#import "UIView+Capture.h"
#import "UIImage+Effects.h"

@interface MKTPopController ()

@property (nonatomic) UIViewController* contentsViewController;
@property (nonatomic, weak) UIView* contentsView;
@property (nonatomic) CGRect contentsDefaultRect;

@property (nonatomic, copy) MKTCompletionHandler completionHandler;
@property (nonatomic, copy) MKTDismissHandler dismissHandler;

@property (nonatomic) UIView* popBackgroundView;
@property (nonatomic) UIView* popOverlayView;
@property (nonatomic) UIImage* capturedImage;
@property (nonatomic) UIImageView* capturedImageView;

@end

@implementation MKTPopController

- (id)init
{
    if (self = [super init]) {
        // initialize default parameters
        self.viewMargin = 10.0f;
        self.viewCornerRadius = 4.0f;
        self.captureMargin = 15.0f;
        
        // initialize views
        self.popBackgroundView = [[UIView alloc] initWithFrame:CGRectZero];
        self.popOverlayView = [[UIView alloc] initWithFrame:CGRectZero];
        self.capturedImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
        
        UITapGestureRecognizer* tapRecognizer = [UITapGestureRecognizer new];
        [tapRecognizer addTarget:self action:@selector(didTapPopOverlayView:)];
        [_popOverlayView setGestureRecognizers:@[tapRecognizer]];
    }
    return self;
}

#pragma mark - Utilities
- (void)doPresentPopContentsView:(UIImage*)blurredCaptureImage
{
    CGRect windowBounds = [[UIScreen mainScreen] bounds];
    CGFloat aspectRatio = windowBounds.size.height / windowBounds.size.width;
    UIWindow* window = [[[UIApplication sharedApplication] delegate] window];;
    
    // initialize animation parameters
    [_popBackgroundView setFrame:windowBounds];
    [_popBackgroundView setBackgroundColor:[UIColor blackColor]];
    [_popBackgroundView setAlpha:1.0f];
    [window addSubview:_popBackgroundView];
    
    [_capturedImageView setFrame:windowBounds];
    [_capturedImageView setAlpha:1.0f];
    [_capturedImageView setImage:blurredCaptureImage];
    [window addSubview:_capturedImageView];
    
    [_popOverlayView setFrame:windowBounds];
    [_popOverlayView setBackgroundColor:[UIColor blackColor]];
    [_popOverlayView setAlpha:0.0f];
    [window addSubview:_popOverlayView];
    
    [_contentsView setFrame:_contentsDefaultRect];
    [_contentsView.layer setCornerRadius:_viewCornerRadius];
    [_contentsView.layer setMasksToBounds:YES];
    [window addSubview:_contentsView];
    
    // notify viewWillAppear to controller
    [_contentsViewController viewWillAppear:YES];
    
    __weak MKTPopController* selfRef = self;
    [UIView animateWithDuration:0.24 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        [selfRef.popOverlayView setAlpha:0.6f];
        
        [selfRef.capturedImageView setFrame:CGRectMake(selfRef.captureMargin,
                                                       selfRef.captureMargin * aspectRatio,
                                                       windowBounds.size.width - selfRef.captureMargin * 2,
                                                       (windowBounds.size.width - selfRef.captureMargin * 2) *  aspectRatio)];
        
        [selfRef.contentsView setFrame:CGRectMake(selfRef.contentsDefaultRect.origin.x,
                                                  windowBounds.size.height - selfRef.contentsDefaultRect.size.height - selfRef.viewMargin,
                                                  selfRef.contentsDefaultRect.size.width,
                                                  selfRef.contentsDefaultRect.size.height)];
    } completion:^(BOOL finished) {
        [selfRef.contentsViewController viewDidAppear:YES];
        
        if (selfRef.completionHandler != nil) {
            selfRef.completionHandler();
            selfRef.completionHandler = nil;
        }
        
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    }];
}

- (void)finishingDismissPopContentsView
{
    [_contentsView removeFromSuperview];
    self.contentsView = nil;
    
    [_popOverlayView removeFromSuperview];
    self.popOverlayView = nil;
    
    [_capturedImageView removeFromSuperview];
    self.capturedImage = nil;
    
    [_popBackgroundView removeFromSuperview];
    self.popBackgroundView = nil;
    
    self.contentsViewController = nil;
    
    if (_dismissHandler != nil) {
        _dismissHandler();
        self.dismissHandler = nil;
    }
}

#pragma mark - API
- (void)setContentsViewController:(UIViewController*)viewController withSize:(CGSize)size
{
    CGRect windowBounds = [[UIScreen mainScreen] bounds];
    
    self.contentsViewController = viewController;
    self.contentsView = viewController.view;
    self.contentsDefaultRect = CGRectMake(_viewMargin, windowBounds.size.height,
                                          size.width - _viewMargin * 2,
                                          size.height);
}

- (void)presentPopControllerWithCompletion:(MKTCompletionHandler)completionHandler
                            DismissHandler:(MKTDismissHandler)dismissHandler
{
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    
    if (completionHandler != nil) {
        self.completionHandler = [completionHandler copy];
    }
    if (dismissHandler != nil) {
        self.dismissHandler = [dismissHandler copy];
    }
    
    __weak MKTPopController* selfRef = self;
    [([[UIApplication sharedApplication] keyWindow]) captureViewWithCompletion:^(UIImage *capturedImage) {
        selfRef.capturedImage = capturedImage;
        
        [selfRef doPresentPopContentsView:[capturedImage blurredImageWithRadius:1.0f]];
    }];
}

- (void)dismissPopController
{
    CGRect windowBounds = [[UIScreen mainScreen] bounds];
    __weak MKTPopController* selfRef = self;
    
    // notify viewWillDisappear to controller
    [_contentsViewController viewWillDisappear:YES];
    
    // set a still normal catpure image.
    [_capturedImageView setImage:_capturedImage];
    
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        [selfRef.popOverlayView setAlpha:0.0f];
        [selfRef.capturedImageView setFrame:CGRectMake(0, 0, windowBounds.size.width, windowBounds.size.height)];
        [selfRef.contentsView setFrame:selfRef.contentsDefaultRect];
    } completion:^(BOOL finished) {
        [selfRef.contentsViewController viewDidDisappear:YES];
        [selfRef finishingDismissPopContentsView];
    }];
}

#pragma mark - Aciton
- (void)didTapPopOverlayView:(id)sender
{
    [self dismissPopController];
}

@end
