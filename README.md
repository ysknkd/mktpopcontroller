# MKTPopController について
MKTPopControllerは、UIViewControllerをポップ表示するシンプルなコントローラーです。

![screenshot](https://dl.dropboxusercontent.com/s/ncilzv41n6c90t6/MKTPopController_ScreenShot.png?dl=1&token_hash=AAGNzdGTLvo93v4WhaQ4LIr9ejFIU16Im_IEqxtb1SkYBQ)

# Unsupported
 - デバイスの回転には対応していません
 - デバイスがランドスケープモードの状態では利用できません
 - ポップするViewControllerに、UINavigationControllerを含むと意図しない表示になります

# Requirements
iOS7.0 or later

# Usage
Import header

    #import "MKTPopController.h"

sample code

    self.popController = [[MKTPopController alloc] init];

    // ポップ表示させたい ViewController を指定します
    // 以下のコードでは、targetViewControlllerがそれに該当します
    [_popController setContentsViewController:targetViewController
                                     withSize:CGSizeMake(320.0f, 180.0f)];
    

    // presentPopControllerWithCompletion で指定した ViewController をポップ表示します
    [_popController presentPopControllerWithCompletion:^{
        // ポップ表示完了後の処理
        ...
    } DismissHandler:^{
        // ポップ非表示後の処理
        ...
    }];

# Customize

    @property (nonatomic) CGFloat viewMargin;
    @property (nonatomic) CGFloat viewCornerRadius;
    @property (nonatomic) CGFloat captureMargin;

